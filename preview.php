<?php

include_once('config.php');

$id = anti_injection($_GET['id']);

?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>
        Preview | Tem Banners Manager
    </title>

    <link href="../../css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="../../css/style.css" rel="stylesheet" />
    <link href="../../node_modules/material-design-icons/iconfont/material-icons.css" rel="stylesheet" />
</head>

<body>
    <div class="site-wrapper">

        <div class="site-wrapper-inner">

                <div class="masthead clearfix">
                    <a href="../../"><img src="../../img/logotag1.png" class="logo" /></a>
                </div>
            <div class="cover-container">

                <div class="inner cover">
                    
                    <div id="bbx" class="bannerBox">
                        <script src="<?=HOST?>adfscript/?id=<?=$id?>&click=http://g1.globo.com/sorocaba?%26"></script>
                    </div>

                    <div id="bi" class="bannerInfo">
                        <span id="id"></span>
                        <span id="desc"></span>
                        <span id="size"></span>
                    </div>

                </div>


                <div class="mastfoot">
                    <div class="inner">
                        <a href="../../"><img src="../../img/logotag1.png" /><br /></a>
                        <p>TAG1 | TVTEM &copy;.</p>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <script type="text/javascript" src="../../node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/popper.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/core.js"></script>

    
    <script>
        $(document).ready(function(){
            $.get('<?=HOST?>rest/getbanner.php?id=<?=$id?>', function(data){
                
                $('#bbx').css({
                    width: data.width,
                    height: data.height
                });

                $('#id').html('ID: ' + data.id);
                $('#desc').html('Descrição: ' + data.descricao);
                $('#size').html('Tamanho: ' + data.width + 'x' + data.height);
            });
        });
    </script>
</body>

</html>