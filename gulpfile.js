var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename');


var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


// Static Server + watching scss/html files
gulp.task('serve', ['compileStyles'], function() {

    browserSync.init({
        server: "./"
    });

    var watcher = gulp.watch("sass/*.{sass,scss}", ['compileStyles']);
    gulp.watch("*.html").on('change', browserSync.reload);
    gulp.watch("js/*.js").on('change', browserSync.reload);
    gulp.watch("css/*.css").on('change', browserSync.reload);


    watcher.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

function processSass(gulp, file) {
    gulp.src('sass/' + file + '.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename(file + '.css'))
        .pipe(gulp.dest('css/'))
        .pipe(prefix('last 3 version'))
        .pipe(minifycss());
}

gulp.task('compileStyles', function() {

    processSass(gulp, 'fonts');
    processSass(gulp, 'bannertags');
    processSass(gulp, 'style');

    gulp.src('./')
        .pipe(notify("Live Reload Ready"));
});

gulp.task('default', ['serve']);