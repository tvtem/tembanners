(function() {
    function it(t, i) { i.src ? n.ajax({ url: i.src, async: !1, dataType: "script" }) : n.globalEval(i.text || i.textContent || i.innerHTML || ""), i.parentNode && i.parentNode.removeChild(i) }

    function f() { return +new Date }

    function s(t, i) { return t[0] && parseInt(n.curCSS(t[0], i, !0), 10) || 0 }

    function l() { return !1 }

    function a() { return !0 }

    function tt(t) {
        var u = RegExp("(^|\\.)" + t.type + "(\\.|$)"),
            r = !0,
            i = [];
        return n.each(n.data(this, "events").live || [], function(r, f) {
            if (u.test(f.type)) {
                var e = n(t.target).closest(f.data)[0];
                e && i.push({ elem: e, fn: f })
            }
        }), i.sort(function(t, i) { return n.data(t.elem, "closest") - n.data(i.elem, "closest") }), n.each(i, function() { if (this.fn.call(this.elem, t, this.fn.data) === !1) return r = !1 }), r
    }

    function b(n, t) { return ["live", n, t.replace(/\./g, "`").replace(/ /g, "|")].join(".") }

    function k() {
        if (y) return;
        y = !0, document.addEventListener ? document.addEventListener("DOMContentLoaded", function() { document.removeEventListener("DOMContentLoaded", arguments.callee, !1), n.ready() }, !1) : document.attachEvent && (document.attachEvent("onreadystatechange", function() { document.readyState === "complete" && (document.detachEvent("onreadystatechange", arguments.callee), n.ready()) }), document.documentElement.doScroll && i == i.top && (function() {
            if (n.isReady) return;
            try { document.documentElement.doScroll("left") } catch (t) { setTimeout(arguments.callee, 0); return }
            n.ready()
        })()), n.event.add(i, "load", n.ready)
    }

    function e(t, i) { var r = {}; return n.each(v.concat.apply([], v.slice(0, i)), function() { r[this] = t }), r }
    var i = this,
        t, et = i.jQuery,
        st = i.$,
        n = i.jQuery = i.$ = function(t, i) { return new n.fn.init(t, i) },
        ut = /^[^<]*(<(.|\s)+>)[^>]*$|^#([\w-]+)$/,
        rt = /^.[^:#\[\.,]*$/,
        u, p, y, o, d, h, c, v;
    n.fn = n.prototype = {
        init: function(t, i) {
            var r, f, u;
            t = t || document;
            if (t.nodeType) return this[0] = t, this.length = 1, this.context = t, this;
            if (typeof t == "string") {
                r = ut.exec(t);
                if (r && (r[1] || !i))
                    if (r[1]) t = n.clean([r[1]], i);
                    else return f = document.getElementById(r[3]), f && f.id != r[3] ? n().find(t) : (u = n(f || []), u.context = document, u.selector = t, u);
                else return n(i).find(t)
            } else if (n.isFunction(t)) return n(document).ready(t);
            return t.selector && t.context && (this.selector = t.selector, this.context = t.context), this.setArray(n.isArray(t) ? t : n.makeArray(t))
        },
        selector: "",
        jquery: "1.3.2",
        size: function() { return this.length },
        get: function(n) { return n === t ? Array.prototype.slice.call(this) : this[n] },
        pushStack: function(t, i, r) { var u = n(t); return u.prevObject = this, u.context = this.context, i === "find" ? u.selector = this.selector + (this.selector ? " " : "") + r : i && (u.selector = this.selector + "." + i + "(" + r + ")"), u },
        setArray: function(n) { return this.length = 0, Array.prototype.push.apply(this, n), this },
        each: function(t, i) { return n.each(this, t, i) },
        index: function(t) { return n.inArray(t && t.jquery ? t[0] : t, this) },
        attr: function(i, r, u) {
            var f = i;
            if (typeof i == "string") {
                if (r === t) return this[0] && n[u || "attr"](this[0], i);
                f = {}, f[i] = r
            }
            return this.each(function(t) { for (i in f) n.attr(u ? this.style : this, i, n.prop(this, f[i], u, t, i)) })
        },
        css: function(n, i) { return (n == "width" || n == "height") && parseFloat(i) < 0 && (i = t), this.attr(n, i, "curCSS") },
        text: function(t) { if (typeof t != "object" && t != null) return this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(t)); var i = ""; return n.each(t || this, function() { n.each(this.childNodes, function() { this.nodeType != 8 && (i += this.nodeType != 1 ? this.nodeValue : n.fn.text([this])) }) }), i },
        wrapAll: function(t) {
            if (this[0]) {
                var i = n(t, this[0].ownerDocument).clone();
                this[0].parentNode && i.insertBefore(this[0]), i.map(function() { var n = this; while (n.firstChild) n = n.firstChild; return n }).append(this)
            }
            return this
        },
        wrapInner: function(t) { return this.each(function() { n(this).contents().wrapAll(t) }) },
        wrap: function(t) { return this.each(function() { n(this).wrapAll(t) }) },
        append: function() { return this.domManip(arguments, !0, function(n) { this.nodeType == 1 && this.appendChild(n) }) },
        prepend: function() { return this.domManip(arguments, !0, function(n) { this.nodeType == 1 && this.insertBefore(n, this.firstChild) }) },
        before: function() { return this.domManip(arguments, !1, function(n) { this.parentNode.insertBefore(n, this) }) },
        after: function() { return this.domManip(arguments, !1, function(n) { this.parentNode.insertBefore(n, this.nextSibling) }) },
        end: function() { return this.prevObject || n([]) },
        push: [].push,
        sort: [].sort,
        splice: [].splice,
        find: function(t) { if (this.length === 1) { var i = this.pushStack([], "find", t); return i.length = 0, n.find(t, this[0], i), i } return this.pushStack(n.unique(n.map(this, function(i) { return n.find(t, i) })), "find", t) },
        clone: function(t) {
            var u = this.map(function() { var t, i; return n.support.noCloneEvent || n.isXMLDoc(this) ? this.cloneNode(!0) : (t = this.outerHTML, t || (i = this.ownerDocument.createElement("div"), i.appendChild(this.cloneNode(!0)), t = i.innerHTML), n.clean([t.replace(/ jQuery\d+="(?:\d+|null)"/g, "").replace(/^\s*/, "")])[0]) }),
                r, i;
            return t === !0 && (r = this.find("*").andSelf(), i = 0, u.find("*").andSelf().each(function() {
                var u, t, f;
                if (this.nodeName !== r[i].nodeName) return;
                u = n.data(r[i], "events");
                for (t in u)
                    for (f in u[t]) n.event.add(this, t, u[t][f], u[t][f].data);
                i++
            })), u
        },
        filter: function(t) { return this.pushStack(n.isFunction(t) && n.grep(this, function(n, i) { return t.call(n, i) }) || n.multiFilter(t, n.grep(this, function(n) { return n.nodeType === 1 })), "filter", t) },
        closest: function(t) {
            var r = n.expr.match.POS.test(t) ? n(t) : null,
                i = 0;
            return this.map(function() {
                var u = this;
                while (u && u.ownerDocument) {
                    if (r ? r.index(u) > -1 : n(u).is(t)) return n.data(u, "closest", i), u;
                    u = u.parentNode, i++
                }
            })
        },
        not: function(i) {
            if (typeof i == "string") {
                if (rt.test(i)) return this.pushStack(n.multiFilter(i, this, !0), "not", i);
                i = n.multiFilter(i, this)
            }
            var r = i.length && i[i.length - 1] !== t && !i.nodeType;
            return this.filter(function() { return r ? n.inArray(this, i) < 0 : this != i })
        },
        add: function(t) { return this.pushStack(n.unique(n.merge(this.get(), typeof t == "string" ? n(t) : n.makeArray(t)))) },
        is: function(t) { return !!t && n.multiFilter(t, this).length > 0 },
        hasClass: function(n) { return !!n && this.is("." + n) },
        val: function(i) {
            var r, u, h, e;
            if (i === t) {
                r = this[0];
                if (r) {
                    if (n.nodeName(r, "option")) return (r.attributes.value || {}).specified ? r.value : r.text;
                    if (n.nodeName(r, "select")) {
                        var o = r.selectedIndex,
                            c = [],
                            s = r.options,
                            f = r.type == "select-one";
                        if (o < 0) return null;
                        for (u = f ? o : 0, h = f ? o + 1 : s.length; u < h; u++) {
                            e = s[u];
                            if (e.selected) {
                                i = n(e).val();
                                if (f) return i;
                                c.push(i)
                            }
                        }
                        return c
                    }
                    return (r.value || "").replace(/\r/g, "")
                }
                return t
            }
            return typeof i == "number" && (i += ""), this.each(function() {
                if (this.nodeType != 1) return;
                if (n.isArray(i) && /radio|checkbox/.test(this.type)) this.checked = n.inArray(this.value, i) >= 0 || n.inArray(this.name, i) >= 0;
                else if (n.nodeName(this, "select")) {
                    var t = n.makeArray(i);
                    n("option", this).each(function() { this.selected = n.inArray(this.value, t) >= 0 || n.inArray(this.text, t) >= 0 }), t.length || (this.selectedIndex = -1)
                } else this.value = i
            })
        },
        html: function(n) { return n === t ? this[0] ? this[0].innerHTML.replace(/ jQuery\d+="(?:\d+|null)"/g, "") : null : this.empty().append(n) },
        replaceWith: function(n) { return this.after(n).remove() },
        eq: function(n) { return this.slice(n, +n + 1) },
        slice: function() { return this.pushStack(Array.prototype.slice.apply(this, arguments), "slice", Array.prototype.slice.call(arguments).join(",")) },
        map: function(t) { return this.pushStack(n.map(this, function(n, i) { return t.call(n, i, n) })) },
        andSelf: function() { return this.add(this.prevObject) },
        domManip: function(t, i, r) {
            function h(t, r) { return i && n.nodeName(t, "table") && n.nodeName(r, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t }
            var u, e;
            if (this[0]) {
                var f = (this[0].ownerDocument || this[0]).createDocumentFragment(),
                    o = n.clean(t, this[0].ownerDocument || this[0], f),
                    s = f.firstChild;
                if (s)
                    for (u = 0, e = this.length; u < e; u++) r.call(h(this[u], s), this.length > 1 || u > 0 ? f.cloneNode(!0) : f);
                o && n.each(o, it)
            }
            return this
        }
    }, n.fn.init.prototype = n.fn, n.extend = n.fn.extend = function() {
        var i = arguments[0] || {},
            u = 1,
            s = arguments.length,
            e = !1,
            o, f, h, r;
        for (typeof i == "boolean" && (e = i, i = arguments[1] || {}, u = 2), typeof i == "object" || n.isFunction(i) || (i = {}), s == u && (i = this, --u); u < s; u++)
            if ((o = arguments[u]) != null)
                for (f in o) {
                    h = i[f], r = o[f];
                    if (i === r) continue;
                    e && r && typeof r == "object" && !r.nodeType ? i[f] = n.extend(e, h || (r.length != null ? [] : {}), r) : r !== t && (i[f] = r)
                }
        return i
    };
    var ft = /z-?index|font-?weight|opacity|zoom|line-?height/i,
        g = document.defaultView || {},
        w = Object.prototype.toString;
    n.extend({
        noConflict: function(t) { return i.$ = st, t && (i.jQuery = et), n },
        isFunction: function(n) { return w.call(n) === "[object Function]" },
        isArray: function(n) { return w.call(n) === "[object Array]" },
        isXMLDoc: function(t) { return t.nodeType === 9 && t.documentElement.nodeName !== "HTML" || !!t.ownerDocument && n.isXMLDoc(t.ownerDocument) },
        globalEval: function(t) {
            if (t && /\S/.test(t)) {
                var r = document.getElementsByTagName("head")[0] || document.documentElement,
                    i = document.createElement("script");
                i.type = "text/javascript", n.support.scriptEval ? i.appendChild(document.createTextNode(t)) : i.text = t, r.insertBefore(i, r.firstChild), r.removeChild(i)
            }
        },
        nodeName: function(n, t) { return n.nodeName && n.nodeName.toUpperCase() == t.toUpperCase() },
        each: function(n, i, r) {
            var u, f = 0,
                o = n.length,
                e;
            if (r) {
                if (o === t) {
                    for (u in n)
                        if (i.apply(n[u], r) === !1) break
                } else
                    for (; f < o;)
                        if (i.apply(n[f++], r) === !1) break
            } else if (o === t) {
                for (u in n)
                    if (i.call(n[u], u, n[u]) === !1) break
            } else
                for (e = n[0]; f < o && i.call(e, f, e) !== !1; e = n[++f]);
            return n
        },
        prop: function(t, i, r, u, f) { return n.isFunction(i) && (i = i.call(t, u)), typeof i == "number" && r == "curCSS" && !ft.test(f) ? i + "px" : i },
        className: { add: function(t, i) { n.each((i || "").split(/\s+/), function(i, r) { t.nodeType != 1 || n.className.has(t.className, r) || (t.className += (t.className ? " " : "") + r) }) }, remove: function(i, r) { i.nodeType == 1 && (i.className = r !== t ? n.grep(i.className.split(/\s+/), function(t) { return !n.className.has(r, t) }).join(" ") : "") }, has: function(t, i) { return t && n.inArray(i, (t.className || t).toString().split(/\s+/)) > -1 } },
        swap: function(n, t, i) {
            var u = {},
                r;
            for (r in t) u[r] = n.style[r], n.style[r] = t[r];
            i.call(n);
            for (r in t) n.style[r] = u[r]
        },
        css: function(t, i, r, u) {
            if (i == "width" || i == "height") {
                var f, o = { position: "absolute", visibility: "hidden", display: "block" },
                    s = i == "width" ? ["Left", "Right"] : ["Top", "Bottom"];

                function e() {
                    f = i == "width" ? t.offsetWidth : t.offsetHeight;
                    if (u === "border") return;
                    n.each(s, function() { u || (f -= parseFloat(n.curCSS(t, "padding" + this, !0)) || 0), u === "margin" ? f += parseFloat(n.curCSS(t, "margin" + this, !0)) || 0 : f -= parseFloat(n.curCSS(t, "border" + this + "Width", !0)) || 0 })
                }
                return t.offsetWidth !== 0 ? e() : n.swap(t, o, e), Math.max(0, Math.round(f))
            }
            return n.curCSS(t, i, r)
        },
        curCSS: function(t, i, r) {
            var u, f = t.style,
                e, c, h, s;
            return i == "opacity" && !n.support.opacity ? (u = n.attr(f, "opacity"), u == "" ? "1" : u) : (i.match(/float/i) && (i = o), !r && f && f[i] ? u = f[i] : g.getComputedStyle ? (i.match(/float/i) && (i = "float"), i = i.replace(/([A-Z])/g, "-$1").toLowerCase(), e = g.getComputedStyle(t, null), e && (u = e.getPropertyValue(i)), i == "opacity" && u == "" && (u = "1")) : t.currentStyle && (c = i.replace(/\-(\w)/g, function(n, t) { return t.toUpperCase() }), u = t.currentStyle[i] || t.currentStyle[c], !/^\d+(px)?$/i.test(u) && /^\d/.test(u) && (h = f.left, s = t.runtimeStyle.left, t.runtimeStyle.left = t.currentStyle.left, f.left = u || 0, u = f.pixelLeft + "px", f.left = h, t.runtimeStyle.left = s)), u)
        },
        clean: function(t, i, r) {
            var o, f;
            i = i || document, typeof i.createElement == "undefined" && (i = i.ownerDocument || i[0] && i[0].ownerDocument || document);
            if (!r && t.length === 1 && typeof t[0] == "string") { o = /^<(\w+)\s*\/?>$/.exec(t[0]); if (o) return [i.createElement(o[1])] }
            var u = [],
                s = [],
                e = i.createElement("div");
            n.each(t, function(t, r) {
                var f, h, c, s, o;
                typeof r == "number" && (r += "");
                if (!r) return;
                if (typeof r == "string") {
                    r = r.replace(/(<(\w+)[^>]*?)\/>/g, function(n, t, i) { return i.match(/^(abbr|br|col|img|input|link|meta|param|hr|area|embed)$/i) ? n : t + "></" + i + ">" }), f = r.replace(/^\s+/, "").substring(0, 10).toLowerCase(), h = !f.indexOf("<opt") && [1, "<select multiple='multiple'>", "</select>"] || !f.indexOf("<leg") && [1, "<fieldset>", "</fieldset>"] || f.match(/^<(thead|tbody|tfoot|colg|cap)/) && [1, "<table>", "</table>"] || !f.indexOf("<tr") && [2, "<table><tbody>", "</tbody></table>"] || (!f.indexOf("<td") || !f.indexOf("<th")) && [3, "<table><tbody><tr>", "</tr></tbody></table>"] || !f.indexOf("<col") && [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"] || !n.support.htmlSerialize && [1, "div<div>", "</div>"] || [0, "", ""], e.innerHTML = h[1] + r + h[2];
                    while (h[0]--) e = e.lastChild;
                    if (!n.support.tbody)
                        for (c = /<tbody/i.test(r), s = !f.indexOf("<table") && !c ? e.firstChild && e.firstChild.childNodes : h[1] == "<table>" && !c ? e.childNodes : [], o = s.length - 1; o >= 0; --o) n.nodeName(s[o], "tbody") && !s[o].childNodes.length && s[o].parentNode.removeChild(s[o]);
                    !n.support.leadingWhitespace && /^\s/.test(r) && e.insertBefore(i.createTextNode(r.match(/^\s*/)[0]), e.firstChild), r = n.makeArray(e.childNodes)
                }
                r.nodeType ? u.push(r) : u = n.merge(u, r)
            });
            if (r) { for (f = 0; u[f]; f++) n.nodeName(u[f], "script") && (!u[f].type || u[f].type.toLowerCase() === "text/javascript") ? s.push(u[f].parentNode ? u[f].parentNode.removeChild(u[f]) : u[f]) : (u[f].nodeType === 1 && u.splice.apply(u, [f + 1, 0].concat(n.makeArray(u[f].getElementsByTagName("script")))), r.appendChild(u[f])); return s }
            return u
        },
        attr: function(i, r, u) {
            var e, f, h, o, s;
            if (!i || i.nodeType == 3 || i.nodeType == 8) return t;
            e = !n.isXMLDoc(i), f = u !== t, r = e && n.props[r] || r;
            if (i.tagName) {
                h = /href|src|style/.test(r), r == "selected" && i.parentNode && i.parentNode.selectedIndex;
                if (r in i && e && !h) {
                    if (f) {
                        if (r == "type" && n.nodeName(i, "input") && i.parentNode) throw "type property can't be changed";
                        i[r] = u
                    }
                    return n.nodeName(i, "form") && i.getAttributeNode(r) ? i.getAttributeNode(r).nodeValue : r == "tabIndex" ? (o = i.getAttributeNode("tabIndex"), o && o.specified ? o.value : i.nodeName.match(/(button|input|object|select|textarea)/i) ? 0 : i.nodeName.match(/^(a|area)$/i) && i.href ? 0 : t) : i[r]
                }
                return !n.support.style && e && r == "style" ? n.attr(i.style, "cssText", u) : (f && i.setAttribute(r, "" + u), s = !n.support.hrefNormalized && e && h ? i.getAttribute(r, 2) : i.getAttribute(r), s === null ? t : s)
            }
            return !n.support.opacity && r == "opacity" ? (f && (i.zoom = 1, i.filter = (i.filter || "").replace(/alpha\([^)]*\)/, "") + (parseInt(u) + "" == "NaN" ? "" : "alpha(opacity=" + u * 100 + ")")), i.filter && i.filter.indexOf("opacity=") >= 0 ? parseFloat(i.filter.match(/opacity=([^)]*)/)[1]) / 100 + "" : "") : (r = r.replace(/-([a-z])/ig, function(n, t) { return t.toUpperCase() }), f && (i[r] = u), i[r])
        },
        trim: function(n) { return (n || "").replace(/^\s+|\s+$/g, "") },
        makeArray: function(t) {
            var r = [],
                i;
            if (t != null) {
                i = t.length;
                if (i == null || typeof t == "string" || n.isFunction(t) || t.setInterval) r[0] = t;
                else
                    while (i) r[--i] = t[i]
            }
            return r
        },
        inArray: function(n, t) {
            for (var i = 0, r = t.length; i < r; i++)
                if (t[i] === n) return i;
            return -1
        },
        merge: function(t, i) {
            var f = 0,
                r, u = t.length;
            if (n.support.getAll)
                while ((r = i[f++]) != null) t[u++] = r;
            else
                while ((r = i[f++]) != null) r.nodeType != 8 && (t[u++] = r);
            return t
        },
        unique: function(t) {
            var r = [],
                e = {},
                i, f, u;
            try { for (i = 0, f = t.length; i < f; i++) u = n.data(t[i]), e[u] || (e[u] = !0, r.push(t[i])) } catch (o) { r = t }
            return r
        },
        grep: function(n, t, i) { for (var f = [], r = 0, u = n.length; r < u; r++) !i != !t(n[r], r) && f.push(n[r]); return f },
        map: function(n, t) { for (var r = [], u, i = 0, f = n.length; i < f; i++) u = t(n[i], i), u != null && (r[r.length] = u); return r.concat.apply([], r) }
    }), u = navigator.userAgent.toLowerCase(), n.browser = { version: (u.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [0, "0"])[1], safari: /webkit/.test(u), opera: /opera/.test(u), msie: /msie/.test(u) && !/opera/.test(u), mozilla: /mozilla/.test(u) && !/(compatible|webkit)/.test(u) }, n.each({ parent: function(n) { return n.parentNode }, parents: function(t) { return n.dir(t, "parentNode") }, next: function(t) { return n.nth(t, 2, "nextSibling") }, prev: function(t) { return n.nth(t, 2, "previousSibling") }, nextAll: function(t) { return n.dir(t, "nextSibling") }, prevAll: function(t) { return n.dir(t, "previousSibling") }, siblings: function(t) { return n.sibling(t.parentNode.firstChild, t) }, children: function(t) { return n.sibling(t.firstChild) }, contents: function(t) { return n.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : n.makeArray(t.childNodes) } }, function(t, i) { n.fn[t] = function(r) { var u = n.map(this, i); return r && typeof r == "string" && (u = n.multiFilter(r, u)), this.pushStack(n.unique(u), t, r) } }), n.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function(t, i) { n.fn[t] = function(r) { for (var e = [], o = n(r), f, u = 0, s = o.length; u < s; u++) f = (u > 0 ? this.clone(!0) : this).get(), n.fn[i].apply(n(o[u]), f), e = e.concat(f); return this.pushStack(e, t, r) } }), n.each({
        removeAttr: function(t) { n.attr(this, t, ""), this.nodeType == 1 && this.removeAttribute(t) },
        addClass: function(t) { n.className.add(this, t) },
        removeClass: function(t) { n.className.remove(this, t) },
        toggleClass: function(t, i) { typeof i != "boolean" && (i = !n.className.has(this, t)), n.className[i ? "add" : "remove"](this, t) },
        remove: function(t) {
            (!t || n.filter(t, [this]).length) && (n("*", this).add([this]).each(function() { n.event.remove(this), n.removeData(this) }), this.parentNode && this.parentNode.removeChild(this))
        },
        empty: function() { n(this).children().remove(); while (this.firstChild) this.removeChild(this.firstChild) }
    }, function(t, i) { n.fn[t] = function() { return this.each(i, arguments) } });
    var r = "jQuery" + f(),
        ot = 0,
        nt = {};
    n.extend({
        cache: {},
        data: function(u, f, e) { u = u == i ? nt : u; var o = u[r]; return o || (o = u[r] = ++ot), f && !n.cache[o] && (n.cache[o] = {}), e !== t && (n.cache[o][f] = e), f ? n.cache[o][f] : o },
        removeData: function(t, u) {
            t = t == i ? nt : t;
            var f = t[r];
            if (u) {
                if (n.cache[f]) {
                    delete n.cache[f][u], u = "";
                    for (u in n.cache[f]) break;
                    u || n.removeData(t)
                }
            } else {
                try { delete t[r] } catch (e) { t.removeAttribute && t.removeAttribute(r) }
                delete n.cache[f]
            }
        },
        queue: function(t, i, r) { if (t) { i = (i || "fx") + "queue"; var u = n.data(t, i);!u || n.isArray(r) ? u = n.data(t, i, n.makeArray(r)) : r && u.push(r) } return u },
        dequeue: function(i, r) {
            var f = n.queue(i, r),
                u = f.shift();
            r && r !== "fx" || (u = f[0]), u !== t && u.call(i)
        }
    }), n.fn.extend({
        data: function(i, r) {
            var u = i.split("."),
                f;
            return u[1] = u[1] ? "." + u[1] : "", r === t ? (f = this.triggerHandler("getData" + u[1] + "!", [u[0]]), f === t && this.length && (f = n.data(this[0], i)), f === t && u[1] ? this.data(u[0]) : f) : this.trigger("setData" + u[1] + "!", [u[0], r]).each(function() { n.data(this, i, r) })
        },
        removeData: function(t) { return this.each(function() { n.removeData(this, t) }) },
        queue: function(i, r) {
            return typeof i != "string" && (r = i, i = "fx"), r === t ? n.queue(this[0], i) : this.each(function() {
                var t = n.queue(this, i, r);
                i == "fx" && t.length == 1 && t[0].call(this)
            })
        },
        dequeue: function(t) { return this.each(function() { n.dequeue(this, t) }) }
    }), (function() {
        function p(n, t, i, r, u, f) {
            for (var c = n == "previousSibling" && !f, e, s, o = 0, h = r.length; o < h; o++) {
                e = r[o];
                if (e) {
                    c && e.nodeType === 1 && (e.sizcache = i, e.sizset = o), e = e[n], s = !1;
                    while (e) {
                        if (e.sizcache === i) { s = r[e.sizset]; break }
                        e.nodeType !== 1 || f || (e.sizcache = i, e.sizset = o);
                        if (e.nodeName === t) { s = e; break }
                        e = e[n]
                    }
                    r[o] = s
                }
            }
        }

        function l(n, t, i, r, f, e) {
            for (var l = n == "previousSibling" && !e, o, h, s = 0, c = r.length; s < c; s++) {
                o = r[s];
                if (o) {
                    l && o.nodeType === 1 && (o.sizcache = i, o.sizset = s), o = o[n], h = !1;
                    while (o) {
                        if (o.sizcache === i) { h = r[o.sizset]; break }
                        if (o.nodeType === 1) { e || (o.sizcache = i, o.sizset = s); if (typeof t != "string") { if (o === t) { h = !0; break } } else if (u.filter(t, [o]).length > 0) { h = o; break } }
                        o = o[n]
                    }
                    r[s] = h
                }
            }
        }
        var s = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?/g,
            c = 0,
            v = Object.prototype.toString,
            u = function(n, t, i, h) {
                var l, nt, b, p, rt, ut, tt, it, g, k, d, c;
                i = i || [], t = t || document;
                if (t.nodeType !== 1 && t.nodeType !== 9) return [];
                if (!n || typeof n != "string") return i;
                l = [], it = !0, s.lastIndex = 0;
                while ((nt = s.exec(n)) !== null) { l.push(nt[1]); if (nt[2]) { tt = RegExp.rightContext; break } }
                if (l.length > 1 && y.exec(n))
                    if (l.length === 2 && r.relative[l[0]]) b = a(l[0] + l[1], t);
                    else { b = r.relative[l[0]] ? [t] : u(l.shift(), t); while (l.length) n = l.shift(), r.relative[n] && (n += l.shift()), b = a(n, b) }
                else { g = h ? { expr: l.pop(), set: f(h) } : u.find(l.pop(), l.length === 1 && t.parentNode ? t.parentNode : t, e(t)), b = u.filter(g.expr, g.set), l.length > 0 ? p = f(b) : it = !1; while (l.length) k = l.pop(), d = k, r.relative[k] ? d = l.pop() : k = "", d == null && (d = t), r.relative[k](p, d, e(t)) }
                p || (p = b);
                if (!p) throw "Syntax error, unrecognized expression: " + (k || n);
                if (v.call(p) === "[object Array]")
                    if (it)
                        if (t.nodeType === 1)
                            for (c = 0; p[c] != null; c++) p[c] && (p[c] === !0 || p[c].nodeType === 1 && w(t, p[c])) && i.push(b[c]);
                        else
                            for (c = 0; p[c] != null; c++) p[c] && p[c].nodeType === 1 && i.push(b[c]);
                else i.push.apply(i, p);
                else f(p, i);
                if (tt) {
                    u(tt, t, i, h);
                    if (o) {
                        hasDuplicate = !1, i.sort(o);
                        if (hasDuplicate)
                            for (c = 1; c < i.length; c++) i[c] === i[c - 1] && i.splice(c--, 1)
                    }
                }
                return i
            },
            r, y, h, f, o;
        u.matches = function(n, t) { return u(n, null, null, t) }, u.find = function(n, t, i) { var u, e, h, f, o, s; if (!n) return []; for (e = 0, h = r.order.length; e < h; e++) { f = r.order[e]; if (o = r.match[f].exec(n)) { s = RegExp.leftContext; if (s.substr(s.length - 1) !== "\\") { o[1] = (o[1] || "").replace(/\\/g, ""), u = r.find[f](o, t, i); if (u != null) { n = n.replace(r.match[f], ""); break } } } } return u || (u = t.getElementsByTagName("*")), { set: u, expr: n } }, u.filter = function(n, i, u, f) {
            var b = n,
                v = [],
                o = i,
                s, h, k = i && i[0] && e(i[0]),
                c, w, l, y, a, p;
            while (n && i.length) {
                for (c in r.filter)
                    if ((s = r.match[c].exec(n)) != null) {
                        w = r.filter[c], h = !1, o == v && (v = []);
                        if (r.preFilter[c]) { s = r.preFilter[c](s, o, u, v, f, k); if (s) { if (s === !0) continue } else h = l = !0 }
                        if (s)
                            for (a = 0;
                                (y = o[a]) != null; a++) y && (l = w(y, s, a, o), p = f ^ !!l, u && l != null ? p ? h = !0 : o[a] = !1 : p && (v.push(y), h = !0));
                        if (l !== t) { u || (o = v), n = n.replace(r.match[c], ""); if (!h) return []; break }
                    }
                if (n == b)
                    if (h == null) throw "Syntax error, unrecognized expression: " + n;
                    else break;
                b = n
            }
            return o
        }, r = u.selectors = {
            order: ["ID", "NAME", "TAG"],
            match: { ID: /#((?:[\w\u00c0-\uFFFF_-]|\\.)+)/, CLASS: /\.((?:[\w\u00c0-\uFFFF_-]|\\.)+)/, NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF_-]|\\.)+)['"]*\]/, ATTR: /\[\s*((?:[\w\u00c0-\uFFFF_-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/, TAG: /^((?:[\w\u00c0-\uFFFF\*_-]|\\.)+)/, CHILD: /:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/, POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/, PSEUDO: /:((?:[\w\u00c0-\uFFFF_-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/ },
            attrMap: { "class": "className", "for": "htmlFor" },
            attrHandle: { href: function(n) { return n.getAttribute("href") } },
            relative: {
                "+": function(n, t, i) {
                    var h = typeof t == "string",
                        o = h && !/\W/.test(t),
                        s = h && !o,
                        f, e, r;
                    for (o && !i && (t = t.toUpperCase()), f = 0, e = n.length; f < e; f++)
                        if (r = n[f]) {
                            while ((r = r.previousSibling) && r.nodeType !== 1);
                            n[f] = s || r && r.nodeName === t ? r || !1 : r === t
                        }
                    s && u.filter(t, n, !0)
                },
                ">": function(n, t, i) {
                    var s = typeof t == "string",
                        o, r, e, f;
                    if (s && !/\W/.test(t))
                        for (t = i ? t : t.toUpperCase(), r = 0, e = n.length; r < e; r++) f = n[r], f && (o = f.parentNode, n[r] = o.nodeName === t ? o : !1);
                    else {
                        for (r = 0, e = n.length; r < e; r++) f = n[r], f && (n[r] = s ? f.parentNode : f.parentNode === t);
                        s && u.filter(t, n, !0)
                    }
                },
                "": function(n, t, i) {
                    var f = c++,
                        u = l,
                        r;
                    t.match(/\W/) || (r = t = i ? t : t.toUpperCase(), u = p), u("parentNode", t, f, n, r, i)
                },
                "~": function(n, t, i) {
                    var f = c++,
                        u = l,
                        r;
                    typeof t != "string" || t.match(/\W/) || (r = t = i ? t : t.toUpperCase(), u = p), u("previousSibling", t, f, n, r, i)
                }
            },
            find: { ID: function(n, t, i) { if (typeof t.getElementById != "undefined" && !i) { var r = t.getElementById(n[1]); return r ? [r] : [] } }, NAME: function(n, t) { var f, u, r, e; if (typeof t.getElementsByName != "undefined") { for (f = [], u = t.getElementsByName(n[1]), r = 0, e = u.length; r < e; r++) u[r].getAttribute("name") === n[1] && f.push(u[r]); return f.length === 0 ? null : f } }, TAG: function(n, t) { return t.getElementsByTagName(n[1]) } },
            preFilter: {
                CLASS: function(n, t, i, r, u, f) {
                    n = " " + n[1].replace(/\\/g, "") + " ";
                    if (f) return n;
                    for (var o = 0, e;
                        (e = t[o]) != null; o++) e && (u ^ (e.className && (" " + e.className + " ").indexOf(n) >= 0) ? i || r.push(e) : i && (t[o] = !1));
                    return !1
                },
                ID: function(n) { return n[1].replace(/\\/g, "") },
                TAG: function(n, t) { for (var i = 0; t[i] === !1; i++); return t[i] && e(t[i]) ? n[1] : n[1].toUpperCase() },
                CHILD: function(n) {
                    if (n[1] == "nth") {
                        var t = /(-?)(\d*)n((?:\+|-)?\d*)/.exec(n[2] == "even" && "2n" || n[2] == "odd" && "2n+1" || !/\D/.test(n[2]) && "0n+" + n[2] || n[2]);
                        n[2] = t[1] + (t[2] || 1) - 0, n[3] = t[3] - 0
                    }
                    return n[0] = c++, n
                },
                ATTR: function(n, t, i, u, f, e) { var o = n[1].replace(/\\/g, ""); return !e && r.attrMap[o] && (n[1] = r.attrMap[o]), n[2] === "~=" && (n[4] = " " + n[4] + " "), n },
                PSEUDO: function(n, t, i, f, e) {
                    if (n[1] === "not")
                        if (n[3].match(s).length > 1 || /^\w/.test(n[3])) n[3] = u(n[3], null, null, t);
                        else { var o = u.filter(n[3], t, i, !0 ^ e); return i || f.push.apply(f, o), !1 }
                    else if (r.match.POS.test(n[0]) || r.match.CHILD.test(n[0])) return !0;
                    return n
                },
                POS: function(n) { return n.unshift(!0), n }
            },
            filters: { enabled: function(n) { return n.disabled === !1 && n.type !== "hidden" }, disabled: function(n) { return n.disabled === !0 }, checked: function(n) { return n.checked === !0 }, selected: function(n) { return n.parentNode.selectedIndex, n.selected === !0 }, parent: function(n) { return !!n.firstChild }, empty: function(n) { return !n.firstChild }, has: function(n, t, i) { return !!u(i[3], n).length }, header: function(n) { return /h\d/i.test(n.nodeName) }, text: function(n) { return "text" === n.type }, radio: function(n) { return "radio" === n.type }, checkbox: function(n) { return "checkbox" === n.type }, file: function(n) { return "file" === n.type }, password: function(n) { return "password" === n.type }, submit: function(n) { return "submit" === n.type }, image: function(n) { return "image" === n.type }, reset: function(n) { return "reset" === n.type }, button: function(n) { return "button" === n.type || n.nodeName.toUpperCase() === "BUTTON" }, input: function(n) { return /input|select|textarea|button/i.test(n.nodeName) } },
            setFilters: { first: function(n, t) { return t === 0 }, last: function(n, t, i, r) { return t === r.length - 1 }, even: function(n, t) { return t % 2 == 0 }, odd: function(n, t) { return t % 2 == 1 }, lt: function(n, t, i) { return t < i[3] - 0 }, gt: function(n, t, i) { return t > i[3] - 0 }, nth: function(n, t, i) { return i[3] - 0 == t }, eq: function(n, t, i) { return i[3] - 0 == t } },
            filter: {
                PSEUDO: function(n, t, i, u) {
                    var e = t[1],
                        s = r.filters[e],
                        f, i, o;
                    if (s) return s(n, i, t, u);
                    if (e === "contains") return (n.textContent || n.innerText || "").indexOf(t[3]) >= 0;
                    if (e === "not") {
                        for (f = t[3], i = 0, o = f.length; i < o; i++)
                            if (f[i] === n) return !1;
                        return !0
                    }
                },
                CHILD: function(n, t) {
                    var s = t[1],
                        i = n,
                        u, e, o, r, h, f;
                    switch (s) {
                        case "only":
                        case "first":
                            while (i = i.previousSibling)
                                if (i.nodeType === 1) return !1;
                            if (s == "first") return !0;
                            i = n;
                        case "last":
                            while (i = i.nextSibling)
                                if (i.nodeType === 1) return !1;
                            return !0;
                        case "nth":
                            u = t[2], e = t[3];
                            if (u == 1 && e == 0) return !0;
                            o = t[0], r = n.parentNode;
                            if (r && (r.sizcache !== o || !n.nodeIndex)) {
                                for (h = 0, i = r.firstChild; i; i = i.nextSibling) i.nodeType === 1 && (i.nodeIndex = ++h);
                                r.sizcache = o
                            }
                            return f = n.nodeIndex - e, u == 0 ? f == 0 : f % u == 0 && f / u >= 0
                    }
                },
                ID: function(n, t) { return n.nodeType === 1 && n.getAttribute("id") === t },
                TAG: function(n, t) { return t === "*" && n.nodeType === 1 || n.nodeName === t },
                CLASS: function(n, t) { return (" " + (n.className || n.getAttribute("class")) + " ").indexOf(t) > -1 },
                ATTR: function(n, t) {
                    var e = t[1],
                        o = r.attrHandle[e] ? r.attrHandle[e](n) : n[e] != null ? n[e] : n.getAttribute(e),
                        u = o + "",
                        f = t[2],
                        i = t[4];
                    return o == null ? f === "!=" : f === "=" ? u === i : f === "*=" ? u.indexOf(i) >= 0 : f === "~=" ? (" " + u + " ").indexOf(i) >= 0 : i ? f === "!=" ? u != i : f === "^=" ? u.indexOf(i) === 0 : f === "$=" ? u.substr(u.length - i.length) === i : f === "|=" ? u === i || u.substr(0, i.length + 1) === i + "-" : !1 : u && o !== !1
                },
                POS: function(n, t, i, u) {
                    var e = t[2],
                        f = r.setFilters[e];
                    if (f) return f(n, i, t, u)
                }
            }
        }, y = r.match.POS;
        for (h in r.match) r.match[h] = RegExp(r.match[h].source + /(?![^\[]*\])(?![^\(]*\))/.source);
        f = function(n, t) { return n = Array.prototype.slice.call(n), t ? (t.push.apply(t, n), t) : n };
        try { Array.prototype.slice.call(document.documentElement.childNodes) } catch (b) {
            f = function(n, t) {
                var r = t || [],
                    u, i;
                if (v.call(n) === "[object Array]") Array.prototype.push.apply(r, n);
                else if (typeof n.length == "number")
                    for (i = 0, u = n.length; i < u; i++) r.push(n[i]);
                else
                    for (i = 0; n[i]; i++) r.push(n[i]);
                return r
            }
        }
        document.documentElement.compareDocumentPosition ? o = function(n, t) { var i = n.compareDocumentPosition(t) & 4 ? -1 : n === t ? 0 : 1; return i === 0 && (hasDuplicate = !0), i } : "sourceIndex" in document.documentElement ? o = function(n, t) { var i = n.sourceIndex - t.sourceIndex; return i === 0 && (hasDuplicate = !0), i } : document.createRange && (o = function(n, t) {
            var u = n.ownerDocument.createRange(),
                r = t.ownerDocument.createRange(),
                i;
            return u.selectNode(n), u.collapse(!0), r.selectNode(t), r.collapse(!0), i = u.compareBoundaryPoints(Range.START_TO_END, r), i === 0 && (hasDuplicate = !0), i
        }), (function() {
            var i = document.createElement("form"),
                u = "script" + +new Date,
                n;
            i.innerHTML = "<input name='" + u + "'/>", n = document.documentElement, n.insertBefore(i, n.firstChild), !document.getElementById(u) || (r.find.ID = function(n, i, r) { if (typeof i.getElementById != "undefined" && !r) { var u = i.getElementById(n[1]); return u ? u.id === n[1] || typeof u.getAttributeNode != "undefined" && u.getAttributeNode("id").nodeValue === n[1] ? [u] : t : [] } }, r.filter.ID = function(n, t) { var i = typeof n.getAttributeNode != "undefined" && n.getAttributeNode("id"); return n.nodeType === 1 && i && i.nodeValue === t }), n.removeChild(i)
        })(), (function() {
            var n = document.createElement("div");
            n.appendChild(document.createComment("")), n.getElementsByTagName("*").length > 0 && (r.find.TAG = function(n, t) {
                var r = t.getElementsByTagName(n[1]),
                    u, i;
                if (n[1] === "*") {
                    for (u = [], i = 0; r[i]; i++) r[i].nodeType === 1 && u.push(r[i]);
                    r = u
                }
                return r
            }), n.innerHTML = "<a href='#'></a>", n.firstChild && typeof n.firstChild.getAttribute != "undefined" && n.firstChild.getAttribute("href") !== "#" && (r.attrHandle.href = function(n) { return n.getAttribute("href", 2) })
        })(), document.querySelectorAll && (function() {
            var n = u,
                t = document.createElement("div");
            t.innerHTML = "<p class='TEST'></p>";
            if (t.querySelectorAll && t.querySelectorAll(".TEST").length === 0) return;
            u = function(t, i, r, u) {
                i = i || document;
                if (!u && i.nodeType === 9 && !e(i)) try { return f(i.querySelectorAll(t), r) } catch (o) {}
                return n(t, i, r, u)
            }, u.find = n.find, u.filter = n.filter, u.selectors = n.selectors, u.matches = n.matches
        })(), document.getElementsByClassName && document.documentElement.getElementsByClassName && (function() {
            var n = document.createElement("div");
            n.innerHTML = "<div class='test e'></div><div class='test'></div>";
            if (n.getElementsByClassName("e").length === 0) return;
            n.lastChild.className = "e";
            if (n.getElementsByClassName("e").length === 1) return;
            r.order.splice(1, 0, "CLASS"), r.find.CLASS = function(n, t, i) { if (typeof t.getElementsByClassName != "undefined" && !i) return t.getElementsByClassName(n[1]) }
        })();
        var w = document.compareDocumentPosition ? function(n, t) { return n.compareDocumentPosition(t) & 16 } : function(n, t) { return n !== t && (n.contains ? n.contains(t) : !0) },
            e = function(n) { return n.nodeType === 9 && n.documentElement.nodeName !== "HTML" || !!n.ownerDocument && e(n.ownerDocument) },
            a = function(n, t) {
                var o = [],
                    s = "",
                    h, f = t.nodeType ? [t] : t,
                    i, e;
                while (h = r.match.PSEUDO.exec(n)) s += h[0], n = n.replace(r.match.PSEUDO, "");
                for (n = r.relative[n] ? n + "*" : n, i = 0, e = f.length; i < e; i++) u(n, f[i], o);
                return u.filter(s, o)
            };
        n.find = u, n.filter = u.filter, n.expr = u.selectors, n.expr[":"] = n.expr.filters, u.selectors.filters.hidden = function(n) { return n.offsetWidth === 0 || n.offsetHeight === 0 }, u.selectors.filters.visible = function(n) { return n.offsetWidth > 0 || n.offsetHeight > 0 }, u.selectors.filters.animated = function(t) { return n.grep(n.timers, function(n) { return t === n.elem }).length }, n.multiFilter = function(n, t, i) { return i && (n = ":not(" + n + ")"), u.matches(n, t) }, n.dir = function(n, t) {
            var r = [],
                i = n[t];
            while (i && i != document) i.nodeType == 1 && r.push(i), i = i[t];
            return r
        }, n.nth = function(n, t, i) {
            t = t || 1;
            for (var u = 0; n; n = n[i])
                if (n.nodeType == 1 && ++u == t) break;
            return n
        }, n.sibling = function(n, t) { for (var i = []; n; n = n.nextSibling) n.nodeType == 1 && n != t && i.push(n); return i };
        return
    })(), n.event = {
        add: function(r, u, f, e) {
            var h, s, o;
            if (r.nodeType == 3 || r.nodeType == 8) return;
            r.setInterval && r != i && (r = i), f.guid || (f.guid = this.guid++), e !== t && (h = f, f = this.proxy(h), f.data = e), s = n.data(r, "events") || n.data(r, "events", {}), o = n.data(r, "handle") || n.data(r, "handle", function() { return typeof n != "undefined" && !n.event.triggered ? n.event.handle.apply(arguments.callee.elem, arguments) : t }), o.elem = r, n.each(u.split(/\s+/), function(t, i) {
                var h = i.split("."),
                    u;
                i = h.shift(), f.type = h.slice().sort().join("."), u = s[i], n.event.specialAll[i] && n.event.specialAll[i].setup.call(r, e, h), u || (u = s[i] = {}, n.event.special[i] && n.event.special[i].setup.call(r, e, h) !== !1 || (r.addEventListener ? r.addEventListener(i, o, !1) : r.attachEvent && r.attachEvent("on" + i, o))), u[f.guid] = f, n.event.global[i] = !0
            }), r = null
        },
        guid: 1,
        global: {},
        remove: function(i, r, u) {
            var f, e, h, s, o;
            if (i.nodeType == 3 || i.nodeType == 8) return;
            f = n.data(i, "events");
            if (f) {
                if (r === t || typeof r == "string" && r.charAt(0) == ".")
                    for (s in f) this.remove(i, s + (r || ""));
                else r.type && (u = r.handler, r = r.type), n.each(r.split(/\s+/), function(t, r) {
                    var o = r.split("."),
                        h, s;
                    r = o.shift(), h = RegExp("(^|\\.)" + o.slice().sort().join(".*\\.") + "(\\.|$)");
                    if (f[r]) {
                        if (u) delete f[r][u.guid];
                        else
                            for (s in f[r]) h.test(f[r][s].type) && delete f[r][s];
                        n.event.specialAll[r] && n.event.specialAll[r].teardown.call(i, o);
                        for (e in f[r]) break;
                        e || (n.event.special[r] && n.event.special[r].teardown.call(i, o) !== !1 || (i.removeEventListener ? i.removeEventListener(r, n.data(i, "handle"), !1) : i.detachEvent && i.detachEvent("on" + r, n.data(i, "handle"))), e = null, delete f[r])
                    }
                });
                for (e in f) break;
                e || (o = n.data(i, "handle"), o && (o.elem = null), n.removeData(i, "events"), n.removeData(i, "handle"))
            }
        },
        trigger: function(i, u, f, e) {
            var o = i.type || i,
                h, s;
            if (!e) {
                i = typeof i == "object" ? i[r] ? i : n.extend(n.Event(o), i) : n.Event(o), o.indexOf("!") < 0 || (i.type = o = o.slice(0, -1), i.exclusive = !0), f || (i.stopPropagation(), this.global[o] && n.each(n.cache, function() { this.events && this.events[o] && n.event.trigger(i, u, this.handle.elem) }));
                if (!f || f.nodeType == 3 || f.nodeType == 8) return t;
                i.result = t, i.target = f, u = n.makeArray(u), u.unshift(i)
            }
            i.currentTarget = f, h = n.data(f, "handle"), h && h.apply(f, u), (!f[o] || n.nodeName(f, "a") && o == "click") && f["on" + o] && f["on" + o].apply(f, u) === !1 && (i.result = !1);
            if (!e && f[o] && !i.isDefaultPrevented() && !(n.nodeName(f, "a") && o == "click")) { this.triggered = !0; try { f[o]() } catch (c) {} }
            this.triggered = !1, i.isPropagationStopped() || (s = f.parentNode || f.ownerDocument, s && n.event.trigger(i, u, s, !0))
        },
        handle: function(r) {
            var s, o, e, h, c, u, f;
            r = arguments[0] = n.event.fix(r || i.event), r.currentTarget = this, e = r.type.split("."), r.type = e.shift(), s = !e.length && !r.exclusive, h = RegExp("(^|\\.)" + e.slice().sort().join(".*\\.") + "(\\.|$)"), o = (n.data(this, "events") || {})[r.type];
            for (c in o) { u = o[c]; if (s || h.test(u.type)) { r.handler = u, r.data = u.data, f = u.apply(this, arguments), f !== t && (r.result = f, f === !1 && (r.preventDefault(), r.stopPropagation())); if (r.isImmediatePropagationStopped()) break } }
        },
        props: "altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode metaKey newValue originalTarget pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),
        fix: function(t) { var e, f, o, i, u; if (t[r]) return t; for (e = t, t = n.Event(e), f = this.props.length; f;) o = this.props[--f], t[o] = e[o]; return t.target || (t.target = t.srcElement || document), t.target.nodeType == 3 && (t.target = t.target.parentNode), !t.relatedTarget && t.fromElement && (t.relatedTarget = t.fromElement == t.target ? t.toElement : t.fromElement), t.pageX == null && t.clientX != null && (i = document.documentElement, u = document.body, t.pageX = t.clientX + (i && i.scrollLeft || u && u.scrollLeft || 0) - (i.clientLeft || 0), t.pageY = t.clientY + (i && i.scrollTop || u && u.scrollTop || 0) - (i.clientTop || 0)), !t.which && (t.charCode || t.charCode === 0 ? t.charCode : t.keyCode) && (t.which = t.charCode || t.keyCode), !t.metaKey && t.ctrlKey && (t.metaKey = t.ctrlKey), !t.which && t.button && (t.which = t.button & 1 ? 1 : t.button & 2 ? 3 : t.button & 4 ? 2 : 0), t },
        proxy: function(n, t) { return t = t || function() { return n.apply(this, arguments) }, t.guid = n.guid = n.guid || t.guid || this.guid++, t },
        special: { ready: { setup: k, teardown: function() {} } },
        specialAll: {
            live: {
                setup: function(t, i) { n.event.add(this, i[0], tt) },
                teardown: function(t) {
                    if (t.length) {
                        var i = 0,
                            r = RegExp("(^|\\.)" + t[0] + "(\\.|$)");
                        n.each(n.data(this, "events").live || {}, function() { r.test(this.type) && i++ }), i < 1 && n.event.remove(this, t[0], tt)
                    }
                }
            }
        }
    }, n.Event = function(t) {
        if (!this.preventDefault) return new n.Event(t);
        t && t.type ? (this.originalEvent = t, this.type = t.type) : this.type = t, this.timeStamp = f(), this[r] = !0
    }, n.Event.prototype = {
        preventDefault: function() {
            this.isDefaultPrevented = a;
            var n = this.originalEvent;
            if (!n) return;
            n.preventDefault && n.preventDefault(), n.returnValue = !1
        },
        stopPropagation: function() {
            this.isPropagationStopped = a;
            var n = this.originalEvent;
            if (!n) return;
            n.stopPropagation && n.stopPropagation(), n.cancelBubble = !0
        },
        stopImmediatePropagation: function() { this.isImmediatePropagationStopped = a, this.stopPropagation() },
        isDefaultPrevented: l,
        isPropagationStopped: l,
        isImmediatePropagationStopped: l
    }, p = function(t) {
        var i = t.relatedTarget;
        while (i && i != this) try { i = i.parentNode } catch (r) { i = this }
        i != this && (t.type = t.data, n.event.handle.apply(this, arguments))
    }, n.each({ mouseover: "mouseenter", mouseout: "mouseleave" }, function(t, i) { n.event.special[i] = { setup: function() { n.event.add(this, t, p, i) }, teardown: function() { n.event.remove(this, t, p) } } }), n.fn.extend({
        bind: function(t, i, r) { return t == "unload" ? this.one(t, i, r) : this.each(function() { n.event.add(this, t, r || i, r && i) }) },
        one: function(t, i, r) { var u = n.event.proxy(r || i, function(t) { return n(this).unbind(t, u), (r || i).apply(this, arguments) }); return this.each(function() { n.event.add(this, t, u, r && i) }) },
        unbind: function(t, i) { return this.each(function() { n.event.remove(this, t, i) }) },
        trigger: function(t, i) { return this.each(function() { n.event.trigger(t, i, this) }) },
        triggerHandler: function(t, i) { if (this[0]) { var r = n.Event(t); return r.preventDefault(), r.stopPropagation(), n.event.trigger(r, i, this[0]), r.result } },
        toggle: function(t) {
            var r = arguments,
                i = 1;
            while (i < r.length) n.event.proxy(t, r[i++]);
            return this.click(n.event.proxy(t, function(n) { return this.lastToggle = (this.lastToggle || 0) % i, n.preventDefault(), r[this.lastToggle++].apply(this, arguments) || !1 }))
        },
        hover: function(n, t) { return this.mouseenter(n).mouseleave(t) },
        ready: function(t) { return k(), n.isReady ? t.call(document, n) : n.readyList.push(t), this },
        live: function(t, i) { var r = n.event.proxy(i); return r.guid += this.selector + t, n(document).bind(b(t, this.selector), this.selector, r), this },
        die: function(t, i) { return n(document).unbind(b(t, this.selector), i ? { guid: i.guid + this.selector + t } : null), this }
    }), n.extend({ isReady: !1, readyList: [], ready: function() { n.isReady || (n.isReady = !0, n.readyList && (n.each(n.readyList, function() { this.call(document, n) }), n.readyList = null), n(document).triggerHandler("ready")) } }), y = !1, n.each("blur,focus,load,resize,scroll,unload,click,dblclick,mousedown,mouseup,mousemove,mouseover,mouseout,mouseenter,mouseleave,change,select,submit,keydown,keypress,keyup,error".split(","), function(t, i) { n.fn[i] = function(n) { return n ? this.bind(i, n) : this.trigger(i) } }), n(i).bind("unload", function() { for (var t in n.cache) t != 1 && n.cache[t].handle && n.event.remove(n.cache[t].handle.elem) }), (function() {
        var f, r;
        n.support = {};
        var e = document.documentElement,
            u = document.createElement("script"),
            t = document.createElement("div"),
            o = "script" + +new Date;
        t.style.display = "none", t.innerHTML = '   <link/><table></table><a href="/a" style="color:red;float:left;opacity:.5;">a</a><select><option>text</option></select><object><param/></object>', f = t.getElementsByTagName("*"), r = t.getElementsByTagName("a")[0];
        if (!f || !f.length || !r) return;
        n.support = { leadingWhitespace: t.firstChild.nodeType == 3, tbody: !t.getElementsByTagName("tbody").length, objectAll: !!t.getElementsByTagName("object")[0].getElementsByTagName("*").length, htmlSerialize: !!t.getElementsByTagName("link").length, style: /red/.test(r.getAttribute("style")), hrefNormalized: r.getAttribute("href") === "/a", opacity: r.style.opacity === "0.5", cssFloat: !!r.style.cssFloat, scriptEval: !1, noCloneEvent: !0, boxModel: null }, u.type = "text/javascript";
        try { u.appendChild(document.createTextNode("window." + o + "=1;")) } catch (s) {}
        e.insertBefore(u, e.firstChild), i[o] && (n.support.scriptEval = !0, delete i[o]), e.removeChild(u), t.attachEvent && t.fireEvent && (t.attachEvent("onclick", function() { n.support.noCloneEvent = !1, t.detachEvent("onclick", arguments.callee) }), t.cloneNode(!0).fireEvent("onclick")), n(function() {
            var t = document.createElement("div");
            t.style.width = t.style.paddingLeft = "1px", document.body.appendChild(t), n.boxModel = n.support.boxModel = t.offsetWidth === 2, document.body.removeChild(t).style.display = "none"
        })
    })(), o = n.support.cssFloat ? "cssFloat" : "styleFloat", n.props = { "for": "htmlFor", "class": "className", float: o, cssFloat: o, styleFloat: o, readonly: "readOnly", maxlength: "maxLength", cellspacing: "cellSpacing", rowspan: "rowSpan", tabindex: "tabIndex" }, n.fn.extend({
        _load: n.fn.load,
        load: function(t, i, r) {
            var u, o, e, f;
            return typeof t != "string" ? this._load(t) : (u = t.indexOf(" "), u < 0 || (o = t.slice(u, t.length), t = t.slice(0, u)), e = "GET", i && (n.isFunction(i) ? (r = i, i = null) : typeof i == "object" && (i = n.param(i), e = "POST")), f = this, n.ajax({
                url: t,
                type: e,
                dataType: "html",
                data: i,
                complete: function(t, i) {
                    (i == "success" || i == "notmodified") && f.html(o ? n("<div/>").append(t.responseText.replace(/<script(.|\s)*?\/script>/g, "")).find(o) : t.responseText), r && f.each(r, [t.responseText, i, t])
                }
            }), this)
        },
        serialize: function() { return n.param(this.serializeArray()) },
        serializeArray: function() { return this.map(function() { return this.elements ? n.makeArray(this.elements) : this }).filter(function() { return this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password|search/i.test(this.type)) }).map(function(t, i) { var r = n(this).val(); return r == null ? null : n.isArray(r) ? n.map(r, function(n) { return { name: i.name, value: n } }) : { name: i.name, value: r } }).get() }
    }), n.each("ajaxStart,ajaxStop,ajaxComplete,ajaxError,ajaxSuccess,ajaxSend".split(","), function(t, i) { n.fn[i] = function(n) { return this.bind(i, n) } }), d = f(), n.extend({
        get: function(t, i, r, u) { return n.isFunction(i) && (r = i, i = null), n.ajax({ type: "GET", url: t, data: i, success: r, dataType: u }) },
        getScript: function(t, i) { return n.get(t, null, i, "script") },
        getJSON: function(t, i, r) { return n.get(t, i, r, "json") },
        post: function(t, i, r, u) { return n.isFunction(i) && (r = i, i = {}), n.ajax({ type: "POST", url: t, data: i, success: r, dataType: u }) },
        ajaxSetup: function(t) { n.extend(n.ajaxSettings, t) },
        ajaxSettings: { url: location.href, global: !0, type: "GET", contentType: "application/x-www-form-urlencoded", processData: !0, async: !0, xhr: function() { return i.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest }, accepts: { xml: "application/xml, text/xml", html: "text/html", script: "text/javascript, application/javascript", json: "application/json, text/javascript", text: "text/plain", _default: "*/*" } },
        lastModified: {},
        ajax: function(r) {
            function w() { r.success && r.success(b, c), r.global && n.event.trigger("ajaxSuccess", [u, r]) }

            function k() { r.complete && r.complete(u, c), r.global && n.event.trigger("ajaxComplete", [u, r]), r.global && !--n.active && n.event.trigger("ajaxStop") }
            var o, h, c, b, s, tt, g, v, a, e, nt, y, u, p, l;
            r = n.extend(!0, r, n.extend(!0, {}, n.ajaxSettings, r)), h = /=\?(&|$)/g, s = r.type.toUpperCase(), r.data && r.processData && typeof r.data != "string" && (r.data = n.param(r.data)), r.dataType == "jsonp" && (s == "GET" ? r.url.match(h) || (r.url += (r.url.match(/\?/) ? "&" : "?") + (r.jsonp || "callback") + "=?") : r.data && r.data.match(h) || (r.data = (r.data ? r.data + "&" : "") + (r.jsonp || "callback") + "=?"), r.dataType = "json"), r.dataType == "json" && (r.data && r.data.match(h) || r.url.match(h)) && (o = "jsonp" + d++, r.data && (r.data = (r.data + "").replace(h, "=" + o + "$1")), r.url = r.url.replace(h, "=" + o + "$1"), r.dataType = "script", i[o] = function(n) {
                b = n, w(), k(), i[o] = t;
                try { delete i[o] } catch (r) {}
                a && a.removeChild(e)
            }), r.dataType == "script" && r.cache == null && (r.cache = !1), r.cache === !1 && s == "GET" && (tt = f(), g = r.url.replace(/(\?|&)_=.*?(&|$)/, "$1_=" + tt + "$2"), r.url = g + (g == r.url ? (r.url.match(/\?/) ? "&" : "?") + "_=" + tt : "")), r.data && s == "GET" && (r.url += (r.url.match(/\?/) ? "&" : "?") + r.data, r.data = null), r.global && !n.active++ && n.event.trigger("ajaxStart"), v = /^(\w+:)?\/\/([^\/?#]+)/.exec(r.url);
            if (r.dataType == "script" && s == "GET" && v && (v[1] && v[1] != location.protocol || v[2] != location.host)) return a = document.getElementsByTagName("head")[0], e = document.createElement("script"), e.src = r.url, r.scriptCharset && (e.charset = r.scriptCharset), o || (nt = !1, e.onload = e.onreadystatechange = function() { nt || this.readyState && this.readyState != "loaded" && this.readyState != "complete" || (nt = !0, w(), k(), e.onload = e.onreadystatechange = null, a.removeChild(e)) }), a.appendChild(e), t;
            y = !1, u = r.xhr(), r.username ? u.open(s, r.url, r.async, r.username, r.password) : u.open(s, r.url, r.async);
            try { r.data && u.setRequestHeader("Content-Type", r.contentType), r.ifModified && u.setRequestHeader("If-Modified-Since", n.lastModified[r.url] || "Thu, 01 Jan 1970 00:00:00 GMT"), u.setRequestHeader("X-Requested-With", "XMLHttpRequest"), u.setRequestHeader("Accept", r.dataType && r.accepts[r.dataType] ? r.accepts[r.dataType] + ", */*" : r.accepts._default) } catch (it) {}
            if (r.beforeSend && r.beforeSend(u, r) === !1) return r.global && !--n.active && n.event.trigger("ajaxStop"), u.abort(), !1;
            r.global && n.event.trigger("ajaxSend", [u, r]), p = function(t) {
                if (u.readyState == 0) l && (clearInterval(l), l = null, r.global && !--n.active && n.event.trigger("ajaxStop"));
                else if (!y && u && (u.readyState == 4 || t == "timeout")) {
                    y = !0, l && (clearInterval(l), l = null), c = t == "timeout" ? "timeout" : n.httpSuccess(u) ? r.ifModified && n.httpNotModified(u, r.url) ? "notmodified" : "success" : "error";
                    if (c == "success") try { b = n.httpData(u, r.dataType, r) } catch (f) { c = "parsererror" }
                    if (c == "success") {
                        var i;
                        try { i = u.getResponseHeader("Last-Modified") } catch (f) {}
                        r.ifModified && i && (n.lastModified[r.url] = i), o || w()
                    } else n.handleError(r, u, c);
                    k(), t && u.abort(), r.async && (u = null)
                }
            }, r.async && (l = setInterval(p, 13), r.timeout > 0 && setTimeout(function() { u && !y && p("timeout") }, r.timeout));
            try { u.send(r.data) } catch (it) { n.handleError(r, u, null, it) }
            return r.async || p(), u
        },
        handleError: function(t, i, r, u) { t.error && t.error(i, r, u), t.global && n.event.trigger("ajaxError", [i, t, u]) },
        active: 0,
        httpSuccess: function(n) { try { return !n.status && location.protocol == "file:" || n.status >= 200 && n.status < 300 || n.status == 304 || n.status == 1223 } catch (t) {} return !1 },
        httpNotModified: function(t, i) { try { var r = t.getResponseHeader("Last-Modified"); return t.status == 304 || r == n.lastModified[i] } catch (u) {} return !1 },
        httpData: function(t, r, u) {
            var e = t.getResponseHeader("content-type"),
                o = r == "xml" || !r && e && e.indexOf("xml") >= 0,
                f = o ? t.responseXML : t.responseText;
            if (o && f.documentElement.tagName == "parsererror") throw "parsererror";
            return u && u.dataFilter && (f = u.dataFilter(f, r)), typeof f == "string" && (r == "script" && n.globalEval(f), r == "json" && (f = i.eval("(" + f + ")"))), f
        },
        param: function(t) {
            function u(n, t) { r[r.length] = encodeURIComponent(n) + "=" + encodeURIComponent(t) }
            var r = [],
                i;
            if (n.isArray(t) || t.jquery) n.each(t, function() { u(this.name, this.value) });
            else
                for (i in t) n.isArray(t[i]) ? n.each(t[i], function() { u(i, this) }) : u(i, n.isFunction(t[i]) ? t[i]() : t[i]);
            return r.join("&").replace(/%20/g, "+")
        }
    }), h = {}, v = [
        ["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"],
        ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"],
        ["opacity"]
    ], n.fn.extend({
        show: function(t, i) { var c, f, u, s, r, o; if (t) return this.animate(e("show", 3), t, i); for (r = 0, o = this.length; r < o; r++) c = n.data(this[r], "olddisplay"), this[r].style.display = c || "", n.css(this[r], "display") === "none" && (f = this[r].tagName, h[f] ? u = h[f] : (s = n("<" + f + " />").appendTo("body"), u = s.css("display"), u === "none" && (u = "block"), s.remove(), h[f] = u), n.data(this[r], "olddisplay", u)); for (r = 0, o = this.length; r < o; r++) this[r].style.display = n.data(this[r], "olddisplay") || ""; return this },
        hide: function(t, i) { var f, r, u; if (t) return this.animate(e("hide", 3), t, i); for (r = 0, u = this.length; r < u; r++) f = n.data(this[r], "olddisplay"), f || f === "none" || n.data(this[r], "olddisplay", n.css(this[r], "display")); for (r = 0, u = this.length; r < u; r++) this[r].style.display = "none"; return this },
        _toggle: n.fn.toggle,
        toggle: function(t, i) {
            var r = typeof t == "boolean";
            return n.isFunction(t) && n.isFunction(i) ? this._toggle.apply(this, arguments) : t == null || r ? this.each(function() {
                var i = r ? t : n(this).is(":hidden");
                n(this)[i ? "show" : "hide"]()
            }) : this.animate(e("toggle", 3), t, i)
        },
        fadeTo: function(n, t, i) { return this.animate({ opacity: t }, n, i) },
        animate: function(t, i, r, u) {
            var f = n.speed(i, r, u);
            return this[f.queue === !1 ? "each" : "queue"](function() {
                var i = n.extend({}, f),
                    r, e = this.nodeType == 1 && n(this).is(":hidden"),
                    u = this;
                for (r in t) {
                    if (t[r] == "hide" && e || t[r] == "show" && !e) return i.complete.call(this);
                    (r == "height" || r == "width") && this.style && (i.display = n.css(this, "display"), i.overflow = this.style.overflow)
                }
                return i.overflow != null && (this.style.overflow = "hidden"), i.curAnim = n.extend({}, t), n.each(t, function(r, f) {
                    var l = new n.fx(u, i, r),
                        h, o, s, c;
                    /toggle|show|hide/.test(f) ? l[f == "toggle" ? e ? "show" : "hide" : f](t) : (h = f.toString().match(/^([+-]=)?([\d+-.]+)(.*)$/), o = l.cur(!0) || 0, h ? (s = parseFloat(h[2]), c = h[3] || "px", c != "px" && (u.style[r] = (s || 1) + c, o = (s || 1) / l.cur(!0) * o, u.style[r] = o + c), h[1] && (s = (h[1] == "-=" ? -1 : 1) * s + o), l.custom(o, s, c)) : l.custom(o, f, ""))
                }), !0
            })
        },
        stop: function(t, i) { var r = n.timers; return t && this.queue([]), this.each(function() { for (var n = r.length - 1; n >= 0; n--) r[n].elem == this && (i && r[n](!0), r.splice(n, 1)) }), i || this.dequeue(), this }
    }), n.each({ slideDown: e("show", 1), slideUp: e("hide", 1), slideToggle: e("toggle", 1), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" } }, function(t, i) { n.fn[t] = function(n, t) { return this.animate(i, n, t) } }), n.extend({ speed: function(t, i, r) { var u = typeof t == "object" ? t : { complete: r || !r && i || n.isFunction(t) && t, duration: t, easing: r && i || i && !n.isFunction(i) && i }; return u.duration = n.fx.off ? 0 : typeof u.duration == "number" ? u.duration : n.fx.speeds[u.duration] || n.fx.speeds._default, u.old = u.complete, u.complete = function() { u.queue !== !1 && n(this).dequeue(), n.isFunction(u.old) && u.old.call(this) }, u }, easing: { linear: function(n, t, i, r) { return i + r * n }, swing: function(n, t, i, r) { return (-Math.cos(n * Math.PI) / 2 + .5) * r + i } }, timers: [], fx: function(n, t, i) { this.options = t, this.elem = n, this.prop = i, t.orig || (t.orig = {}) } }), n.fx.prototype = {
        update: function() { this.options.step && this.options.step.call(this.elem, this.now, this), (n.fx.step[this.prop] || n.fx.step._default)(this), (this.prop == "height" || this.prop == "width") && this.elem.style && (this.elem.style.display = "block") },
        cur: function(t) { if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) return this.elem[this.prop]; var i = parseFloat(n.css(this.elem, this.prop, t)); return i && i > -1e4 ? i : parseFloat(n.curCSS(this.elem, this.prop)) || 0 },
        custom: function(i, r, u) {
            function e(n) { return o.step(n) }
            this.startTime = f(), this.start = i, this.end = r, this.unit = u || this.unit || "px", this.now = this.start, this.pos = this.state = 0;
            var o = this;
            e.elem = this.elem, e() && n.timers.push(e) && !c && (c = setInterval(function() {
                for (var r = n.timers, i = 0; i < r.length; i++) r[i]() || r.splice(i--, 1);
                r.length || (clearInterval(c), c = t)
            }, 13))
        },
        show: function() { this.options.orig[this.prop] = n.attr(this.elem.style, this.prop), this.options.show = !0, this.custom(this.prop == "width" || this.prop == "height" ? 1 : 0, this.cur()), n(this.elem).show() },
        hide: function() { this.options.orig[this.prop] = n.attr(this.elem.style, this.prop), this.options.hide = !0, this.custom(this.cur(), 0) },
        step: function(t) {
            var e = f(),
                u, o, i, r;
            if (t || e >= this.options.duration + this.startTime) {
                this.now = this.end, this.pos = this.state = 1, this.update(), this.options.curAnim[this.prop] = !0, u = !0;
                for (o in this.options.curAnim) this.options.curAnim[o] !== !0 && (u = !1);
                if (u) {
                    this.options.display != null && (this.elem.style.overflow = this.options.overflow, this.elem.style.display = this.options.display, n.css(this.elem, "display") == "none" && (this.elem.style.display = "block")), this.options.hide && n(this.elem).hide();
                    if (this.options.hide || this.options.show)
                        for (i in this.options.curAnim) n.attr(this.elem.style, i, this.options.orig[i]);
                    this.options.complete.call(this.elem)
                }
                return !1
            }
            return r = e - this.startTime, this.state = r / this.options.duration, this.pos = n.easing[this.options.easing || (n.easing.swing ? "swing" : "linear")](this.state, r, 0, 1, this.options.duration), this.now = this.start + (this.end - this.start) * this.pos, this.update(), !0
        }
    }, n.extend(n.fx, { speeds: { slow: 600, fast: 200, _default: 400 }, step: { opacity: function(t) { n.attr(t.elem.style, "opacity", t.now) }, _default: function(n) { n.elem.style && n.elem.style[n.prop] != null ? n.elem.style[n.prop] = n.now + n.unit : n.elem[n.prop] = n.now } } }), n.fn.offset = document.documentElement.getBoundingClientRect ? function() {
        if (!this[0]) return { top: 0, left: 0 };
        if (this[0] === this[0].ownerDocument.body) return n.offset.bodyOffset(this[0]);
        var r = this[0].getBoundingClientRect(),
            u = this[0].ownerDocument,
            t = u.body,
            i = u.documentElement,
            o = i.clientTop || t.clientTop || 0,
            s = i.clientLeft || t.clientLeft || 0,
            f = r.top + (self.pageYOffset || n.boxModel && i.scrollTop || t.scrollTop) - o,
            e = r.left + (self.pageXOffset || n.boxModel && i.scrollLeft || t.scrollLeft) - s;
        return { top: f, left: e }
    } : function() {
        if (!this[0]) return { top: 0, left: 0 };
        if (this[0] === this[0].ownerDocument.body) return n.offset.bodyOffset(this[0]);
        n.offset.initialized || n.offset.initialize();
        var t = this[0],
            o = t.offsetParent,
            l = t,
            h = t.ownerDocument,
            i, s = h.documentElement,
            f = h.body,
            c = h.defaultView,
            e = c.getComputedStyle(t, null),
            u = t.offsetTop,
            r = t.offsetLeft;
        while ((t = t.parentNode) && t !== f && t !== s) i = c.getComputedStyle(t, null), u -= t.scrollTop, r -= t.scrollLeft, t === o && (u += t.offsetTop, r += t.offsetLeft, !n.offset.doesNotAddBorder || n.offset.doesAddBorderForTableAndCells && /^t(able|d|h)$/i.test(t.tagName) || (u += parseInt(i.borderTopWidth, 10) || 0, r += parseInt(i.borderLeftWidth, 10) || 0), l = o, o = t.offsetParent), n.offset.subtractsBorderForOverflowNotVisible && i.overflow !== "visible" && (u += parseInt(i.borderTopWidth, 10) || 0, r += parseInt(i.borderLeftWidth, 10) || 0), e = i;
        return (e.position === "relative" || e.position === "static") && (u += f.offsetTop, r += f.offsetLeft), e.position === "fixed" && (u += Math.max(s.scrollTop, f.scrollTop), r += Math.max(s.scrollLeft, f.scrollLeft)), { top: u, left: r }
    }, n.offset = {
        initialize: function() {
            if (this.initialized) return;
            var n = document.body,
                i = document.createElement("div"),
                t, f, h, e, r, u, o = n.style.marginTop,
                s = '<div style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;"><div></div></div><table style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;" cellpadding="0" cellspacing="0"><tr><td></td></tr></table>';
            r = { position: "absolute", top: 0, left: 0, margin: 0, border: 0, width: "1px", height: "1px", visibility: "hidden" };
            for (u in r) i.style[u] = r[u];
            i.innerHTML = s, n.insertBefore(i, n.firstChild), t = i.firstChild, f = t.firstChild, e = t.nextSibling.firstChild.firstChild, this.doesNotAddBorder = f.offsetTop !== 5, this.doesAddBorderForTableAndCells = e.offsetTop === 5, t.style.overflow = "hidden", t.style.position = "relative", this.subtractsBorderForOverflowNotVisible = f.offsetTop === -5, n.style.marginTop = "1px", this.doesNotIncludeMarginInBodyOffset = n.offsetTop === 0, n.style.marginTop = o, n.removeChild(i), this.initialized = !0
        },
        bodyOffset: function(t) {
            n.offset.initialized || n.offset.initialize();
            var r = t.offsetTop,
                i = t.offsetLeft;
            return n.offset.doesNotIncludeMarginInBodyOffset && (r += parseInt(n.curCSS(t, "marginTop", !0), 10) || 0, i += parseInt(n.curCSS(t, "marginLeft", !0), 10) || 0), { top: r, left: i }
        }
    }, n.fn.extend({
        position: function() {
            var u = 0,
                f = 0,
                r;
            if (this[0]) {
                var n = this.offsetParent(),
                    t = this.offset(),
                    i = /^body|html$/i.test(n[0].tagName) ? { top: 0, left: 0 } : n.offset();
                t.top -= s(this, "marginTop"), t.left -= s(this, "marginLeft"), i.top += s(n, "borderTopWidth"), i.left += s(n, "borderLeftWidth"), r = { top: t.top - i.top, left: t.left - i.left }
            }
            return r
        },
        offsetParent: function() { var t = this[0].offsetParent || document.body; while (t && !/^body|html$/i.test(t.tagName) && n.css(t, "position") == "static") t = t.offsetParent; return n(t) }
    }), n.each(["Left", "Top"], function(r, u) {
        var f = "scroll" + u;
        n.fn[f] = function(u) { return this[0] ? u !== t ? this.each(function() { this == i || this == document ? i.scrollTo(r ? n(i).scrollLeft() : u, r ? u : n(i).scrollTop()) : this[f] = u }) : this[0] == i || this[0] == document ? self[r ? "pageYOffset" : "pageXOffset"] || n.boxModel && document.documentElement[f] || document.body[f] : this[0][f] : null }
    }), n.each(["Height", "Width"], function(r, u) {
        var o = r ? "Left" : "Top",
            s = r ? "Right" : "Bottom",
            e = u.toLowerCase(),
            f;
        n.fn["inner" + u] = function() { return this[0] ? n.css(this[0], e, !1, "padding") : null }, n.fn["outer" + u] = function(t) { return this[0] ? n.css(this[0], e, !1, t ? "margin" : "border") : null }, f = u.toLowerCase(), n.fn[f] = function(r) { return this[0] == i ? document.compatMode == "CSS1Compat" && document.documentElement["client" + u] || document.body["client" + u] : this[0] == document ? Math.max(document.documentElement["client" + u], document.body["scroll" + u], document.documentElement["scroll" + u], document.body["offset" + u], document.documentElement["offset" + u]) : r === t ? this.length ? n.css(this[0], f) : null : this.css(f, typeof r == "string" ? r : r + "px") }
    })
})()