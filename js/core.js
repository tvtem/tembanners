//var $domain = "http://temmais.com/subdomains/testetag1";
var $domain = "https://banners.temmais.com";

$(function() {

    $('[data-toggle="popover"]').popover({
        trigger: 'manual'
    });

    $('#inputID').blur(function() {
        var $id = $(this).val();
        var $this = $(this);

        $.get($domain + '/rest/checkid.php?id=' + $id, function($response) {
            if ($response == "ERR_EXIST") {
                $this.popover('show');
                $this.addClass('err');
            } else {
                $this.popover('hide');
                $this.removeClass('err');
            }
        });
    });

    $('#uploadForm').submit(function() {
        var $this = $(this);

        if ($('#inputID').hasClass('err')) {
            return false;
        }

    });

    loadList();

    $('body').on('DOMSubtreeModified', function(){
        $('[data-clipboard-target]').click(function(){
            var $el = $(this).attr('data-clipboard-target');
            var $tt = $($el + '-tt');

            $($el).CopyToClipboard();

            $tt.tooltip({
                trigger: 'manual'
            });
            $tt.tooltip('show');
            setTimeout(function(){
                $tt.tooltip('hide');
            }, 2000);
            
        });

    });
})

function loadList(){
    $('#list-content').load($domain + '/rest/getlist.php', function(){

        var $ddz = $('.ddzone');
        var $bid = $ddz.attr('data-banner');

        var $ddzone = $ddz.dropzone();
        $ddzone.on("sending", function(file, xhr, formData) {
            // Will send the filesize along with the file as POST data.
            formData.append("bannerId", $bid);
        });
        
        $('#dataTable').DataTable();
    });
}