<?php

include_once('config.php');

?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>
        TAG1
    </title>

    <link href="css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet" />
    <link href="node_modules/material-design-icons/iconfont/material-icons.css" rel="stylesheet" />
</head>

<body>

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <div class="masthead clearfix">
                <a href="/"><img src="img/logotag1.png" class="logo" /></a>
            </div>

            <div class="cover-container">

                <div class="inner cover">
                    <div class="uploadForm">
                        <form action="file-upload.php" class="top-placeholder dropzone" data-modal="#fileModal">
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                            <div class="modal fade" id="fileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 id="exampleModalLabel">Informações Adicionais</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="hidden" name="update" value="-1" />
                                                <input id="" class="form-control inputId" name="id" placeholder="ID" />
                                                <span id="" class="err_id hidden"></span>
                                            </div>
                                            <div class="form-group">
                                                <input id="" class="form-control inputDesc" name="description" placeholder="Descrição" />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button id="" type="button" class="btn btn-primary save-file">Salvar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Banners Cadastrados</h4>
                            <p class="card-text">&nbsp;</p>

                            <table id="dataTable">
                                <thead>
                                    <tr>
                                        <th><strong>#</strong></th>
                                        <th><strong>ID</strong></th>
                                        <th><strong>Descrição</strong></th>
                                        <th><strong>Data</strong></th>
                                        <th><strong>Peso</strong></th>
                                        <th><strong>Ação</strong></th>
                                    </tr>
                                </thead>
                                <tbody id="list-content">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="mastfoot">
                    <div class="inner">
                        <a href="/"><img src="img/logotag1.png" /><br /></a>
                        <p>TAG1 | TVTEM &copy;.</p>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>
    <script src="js/jquery.clipboard.js"></script>
    <script type="text/javascript" src="js/core.js"></script>
    <script src="js/dropzone.js"></script>
    <script type="text/javascript">
        // 86acbd31cd7c09cf30acb66d2fbedc91daa48b86:1508251453.48
        !function(n,t,c,e,u){function r(n){try{f=n(u)}catch(n){return h=n,void i(p,n)}i(s,f)}function i(n,t){for(var c=0;c<n.length;c++)d(n[c],t);
        }function o(n,t){return n&&(f?d(n,f):s.push(n)),t&&(h?d(t,h):p.push(t)),l}function a(n){return o(!1,n)}function d(t,c){
        n.setTimeout(function(){t(c)},0)}var f,h,s=[],p=[],l={then:o,catch:a,_setup:r};n[e]=l;var v=t.createElement("script");
        v.src=c,v.async=!0,v.id="_uasdk",v.rel=e,t.head.appendChild(v)}(window,document,'https://web-sdk.urbanairship.com/notify/v1/ua-sdk.min.js',
        'UA', {
            appKey: 'Q5EHXb6gTRudni6MEdEmjQ',
            token: 'MTpRNUVIWGI2Z1RSdWRuaTZNRWRFbWpROldmZFM3NzBfNko4SzZQYTdqME1zVTRoeHdLYmNJNTlyX3hDWUhMWnR4N2c',
            vapidPublicKey: 'BEyeaY27GpfE7QMwCuxHVJbWTXV7WHqwyzVJ9Argd_nicKupacSm_Kxkekvl8paAaodRIhc3bxXA_11m2C8pwG8='
        });

        $(document).ready(function(){
            UA.then(function(sdk) {
            sdk.register()
            })
        });
    </script>
</body>

</html>