<?php

include_once('config.php');

$GLOBALS['PLATFORM'] = 'GWD';


function detectGWD($body){
    $res = array();

    $content = explode("<div is=\"gwd-page\"", $body);
    if (sizeof($content) < 2) // Não é feito no GWD
        return detectGLB($body);


    $content = $content[1];
    $content = explode("px\" data-gwd-height=\"", $content);

    $w = $content[0];
    $w = explode("data-gwd-width=\"", $w);
    $w = $w[1];


    $h = $content[1];
    $h = explode("px\">", $h);
    $h = $h[0];

    $res = array($w, $h);

    return $res;
}

function detectGLB($body){
    $GLOBALS['PLATFORM'] = 'GLB';

    $res = array();

    $content = explode("bannerSize = [", $body);
    $content = explode("]", $content[1]);

    $res = explode(",", $content[0]);

    return $res;
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = anti_injection($_POST['id']);
    $desc = anti_injection($_POST['description']);
    $update =  anti_injection($_POST['update']) == 1;
    
    if (!$update){
        $stmt = sqlsrv_query( $conn, "SELECT * FROM [dbo].[banners] WHERE [id]='$id'" );  
        if( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))  
        {
			sqlsrv_free_stmt($stmt);  
			sqlsrv_close( $conn );
            die ('ID_USED');
        }
    }

    $time = time('U');
    $size = $_FILES['file']['size'];

    $uploaddir = './received_files/';
    $uploadfile = $uploaddir . basename($_FILES['file']['name']);
    $ext = explode('.', $uploadfile);
    $ext = $ext[sizeof($ext)-1];

    if ($ext != "zip"){
        die("WRONG_FORMAT");
    }

    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {

        $path = './received_files/'.$id;
        $zip = new ZipArchive; 
        $zip->open($uploadfile); 
        $zip->extractTo($path); 
        $zip->close(); 

        unlink($uploadfile);

        $w = 0;
        $h = 0;

        $body = file_get_contents($path . '/index.html');
        $sizes = detectGWD($body);

        $w = $sizes[0];
        $h = $sizes[1];
        
        if (!$update){
            $stmt = sqlsrv_query($conn, "INSERT INTO [dbo].[banners] (id,descricao,registro,peso,usuario,width,height) VALUES ('$id','$desc','$time','$size','0','$w','$h')") or die( print_r( sqlsrv_errors(), true));
        } else {
            $stmt = sqlsrv_query($conn, "UPDATE [dbo].[banners] SET descricao='$desc', registro='$time', peso='$size', width='$w', height='$h' WHERE id='$id'") or die( print_r( sqlsrv_errors(), true));
        }
        
		sqlsrv_free_stmt($stmt);  
		sqlsrv_close($conn); 

        // CORRECTIONS
        if ($GLOBALS['PLATFORM'] == 'GLB'){
            $body = '<style>body{margin:0;padding:0;}</style>'.$body.'{{CLICK_TAG}}';
            file_put_contents($path . '/index.html', $body);
        } else if ($GLOBALS['PLATFORM'] == 'GWD'){
            $body = trim($body);

            $params = explode('<div style="opacity:0;cursor:pointer;position: absolute; z-index: 99999999;" onclick="javascript:window.open(\'%%CLICK_URL_ESC%%%%DEST_URL%%\', \'_blank\'); void(0)">', $body);
            if (sizeof($params) > 1){

                $before = $params[0];

                $params = explode('</canvas>', $params[1]);
                //$params = explode('</div>', $params[1]);

                $after = $params[1];

                $body = $before . '{{CLICK_TAG}}<div>' . $after;
                file_put_contents($path . '/index.html', $body);

            } else {
                $body = str_replace('</body>', '{{CLICK_TAG}}</body>', $body);
                file_put_contents($path . '/index.html', $body);
			}

        }

        die('ALL_OK');

    } else {
        die ("ATTACK_DETECTED");
    }

}



?>