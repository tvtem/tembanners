<?php

include_once('config.php');

?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>
        TAG1
    </title>

    <link href="css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet" />
    <link href="node_modules/material-design-icons/iconfont/material-icons.css" rel="stylesheet" />
    <script type="text/javascript">
    // 86acbd31cd7c09cf30acb66d2fbedc91daa48b86:1508251453.48
    !function(n,t,c,e,u){function r(n){try{f=n(u)}catch(n){return h=n,void i(p,n)}i(s,f)}function i(n,t){for(var c=0;c<n.length;c++)d(n[c],t);
    }function o(n,t){return n&&(f?d(n,f):s.push(n)),t&&(h?d(t,h):p.push(t)),l}function a(n){return o(!1,n)}function d(t,c){
    n.setTimeout(function(){t(c)},0)}var f,h,s=[],p=[],l={then:o,catch:a,_setup:r};n[e]=l;var v=t.createElement("script");
    v.src=c,v.async=!0,v.id="_uasdk",v.rel=e,t.head.appendChild(v)}(window,document,'https://web-sdk.urbanairship.com/notify/v1/ua-sdk.min.js',
    'UA', {
        appKey: 'Q5EHXb6gTRudni6MEdEmjQ',
        token: 'MTpRNUVIWGI2Z1RSdWRuaTZNRWRFbWpROldmZFM3NzBfNko4SzZQYTdqME1zVTRoeHdLYmNJNTlyX3hDWUhMWnR4N2c',
        vapidPublicKey: 'BEyeaY27GpfE7QMwCuxHVJbWTXV7WHqwyzVJ9Argd_nicKupacSm_Kxkekvl8paAaodRIhc3bxXA_11m2C8pwG8='
    });

    document.addEventListener('click', function(){
     UA.then(function(sdk) {
      sdk.register()
     })
   })
    </script>
</head>

<body>

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <div class="masthead clearfix">
                <a href="/"><img src="img/logotag1.png" class="logo" /></a>
            </div>

            <div class="cover-container">

                <div class="inner cover">
                    <div class="row">
                    <div class="col-xs-100 col-md-30"></div>
                    <div class="col-xs-100 col-md-40">
                      <section class="panel">
                      <?php if (isset($_SESSION[SYS_KEY]['err'])) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?=$_SESSION[SYS_KEY]['err']?>
                            </div>
                      <?php unset($_SESSION[SYS_KEY]['err']); } ?>
                          <header class="panel-heading">
                              Login
                          </header>
                          <hr />
                          <div class="panel-body">
                              <form id="loginForm" role="form" action="dologin.php" method="POST">
                                  <div class="form-group">
                                      <input runat="server" name="username" type="text" class="form-control" placeholder="Username">
                                      <input runat="server" name="password" type="password" class="form-control" placeholder="Password">
                                  </div>
                                  <button type="submit" class="btn btn-default">Entrar</button>
                              </form>

                          </div>
                      </section>
                      </div>
                    </div>

                      
                </div>

                <div class="mastfoot">
                    <div class="inner">
                        <a href="/"><img src="img/logotag1.png" /><br /></a>
                        <p>TAG1 | TVTEM &copy;.</p>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>
    <script src="js/jquery.clipboard.js"></script>
    <script type="text/javascript" src="js/core.js"></script>
    <script src="js/dropzone.js"></script>
</body>

</html>