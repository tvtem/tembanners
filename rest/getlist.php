<?php

include_once('../config.php');

$count = 1;
$stmt = sqlsrv_query( $conn, "SELECT * FROM [dbo].[banners] ORDER BY n DESC" );  
while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))  
{
	$date = new DateTime();
	$date->setTimestamp($row['registro']);
	$id = $row['id'];
	$w = $row['width'];
	$h = $row['height'];
	
	$path = '../received_files/'.$id;
	
	$total_size = 0;
	if (is_dir($path)){
		$files = scandir($path);
		foreach ($files as $file){
			if ($file != "." && $file != ".."){
				$s = filesize('../received_files/'.$id.'/'.$file);
				$total_size += $s;
			}
		}
	}
	//data-clipboard-target="#tag1-'.$id.'" 
	echo '<tr class="table">
		<td>'.$count.'</td>
		<td data-toggle="tooltip" data-title="['.$w.'x'.$h.']">'.$id.'</td>
		<td>'   .$row['descricao'].'</td>
		<td>'.$date->format('d-m-Y H:i:s').'</td>
		<td>'.number_format(intval($total_size) / 1024 , 2).'kb</td>
		<td>
		<button id="#tag1-'.$id.'-bt" type="button" data-toggle="collapse" data-target="#collapse'.$id.'" class="btn btn-primary btn-sm"><i id="tag1-'.$id.'-tt" class="material-icons" data-toggle="tooltip" data-placement="top" title="Copiado!">code</i></button>
			<button type="button" data-preview="'.$id.'" class="btn btn-primary btn-sm" onclick="window.open(\''.HOST.'preview/'.$id.'/\', \'_blank\')"><i class="material-icons">pageview</i></button>
			
			<form action="file-upload.php" class="irefresh ddzone dropzone" id="dz'.$id.'" data-banner="'.$id.'" data-modal="#updateModal_'.$id.'">
				<input type="hidden" name="update" value="1" />
				<div class="fallback">
					<input name="file" type="file" multiple />
				</div>
				<div class="modal fade" id="updateModal_'.$id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 id="exampleModalLabel">Informações Adicionais para "'.$id.'"</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<input id="" class="form-control inputId" name="id" value="'.$id.'" disabled="disabled" />
									<span id="" class="err_id hidden"></span>
								</div>
								<div class="form-group">
									<input id="" class="form-control inputDesc" name="description" placeholder="Descrição" />
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button id="" type="button" class="btn btn-primary save-file">Salvar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			
			<div class="collapse" id="collapse'.$id.'">
					<div class="card card-body">
						<input style="width: 97%" id="tag1-'.$id.'" class="form-control" value="&lt;script language=&quot;javascript&quot; src=&quot;'.HOST.'adfscript/?id='.$id.'&click=%%CLICK_URL_ESC%%%%DEST_URL%%&quot;&gt;&lt;/script&gt;" readonly />
					</div>
			</div>
			
			<button type="button" data-preview="'.$id.'" class="btn btn-primary btn-sm" onclick="window.open(\''.HOST.'rest/downloadbanner.php?id='.$id.'/\', \'_blank\')"><i class="material-icons">file_download</i></button>
		</td>
	</tr>';
	$count++;
}

