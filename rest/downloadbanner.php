<?php



include_once('../config.php');

$id = anti_injection($_GET['id']);
$filename = 'temp'.time('U').'.zip';

$folder = "../received_files/".$id;

$zip = new ZipArchive();
$zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($folder),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($folder) + 1);

        // Add current file to archive
        $zip->addFile($filePath, '/'. str_replace("received_files/", "", $name));
    }
}

// Zip archive will be created only after closing object
$zip->close();


// Define headers
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=$filename");
header("Content-Type: application/zip");
header("Content-Transfer-Encoding: binary");

readfile($filename);

unlink($filename);